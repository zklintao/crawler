const Sequelize = require("sequelize");
const DB = require("./db");
const Test = require("./test");

// define category table
const Listening = DB.define("listening", {
  uuid: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "cannot be empty"
      }
    }
  },
  path: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "cannot be empty"
      }
    }
  },
  question: {
    type: Sequelize.TEXT,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "cannot be empty"
      }
    }
  }
}, {
  defaultScope: {
    attributes: {
      exclude: ["updatedAt", "createdAt"]
    }
  }
});

Listening.belongsTo(Test);

// create user table if not exists
Listening.sync({
  force: false
});

module.exports = Listening;
