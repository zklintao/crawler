const Sequelize = require("sequelize");
const sequelize = new Sequelize("postgres://test:123456@localhost:5432/testDB");

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

module.exports = sequelize;
