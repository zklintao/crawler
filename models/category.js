const Sequelize = require("sequelize");
const DB = require("./db");

// define category table
const Category = DB.define("category", {
  uuid: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "cannot be empty"
      }
    }
  }
}, {
  defaultScope: {
    attributes: {
      exclude: ["updatedAt", "createdAt"]
    }
  }
});

// create user table if not exists
Category.sync({
  force: false
});

module.exports = Category;
