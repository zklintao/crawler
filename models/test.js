const Sequelize = require("sequelize");
const DB = require("./db");
const Category = require("./category");

// define category table
const Test = DB.define("test", {
  uuid: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "cannot be empty"
      }
    }
  }
}, {
  defaultScope: {
    attributes: {
      exclude: ["updatedAt", "createdAt"]
    }
  }
});

Test.belongsTo(Category);

// create user table if not exists
Test.sync({
  force: false
});

module.exports = Test;
