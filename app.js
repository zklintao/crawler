const puppeteer = require("puppeteer");
const IeltsListening = require("./crawlers/ielts/listening");
const SATReading = require("./crawlers/sat/reading");
const SATWriting = require("./crawlers/sat/writing");
const SATMath = require("./crawlers/sat/math");
const SATEssay = require("./crawlers/sat/essay");

(async() => {
  // const browser = await puppeteer.launch();
  // const ieltsListening = IeltsListening(browser);
  // const data = await ieltsListening("http://ielts.kmf.com/question/2652sk.html");
  // await browser.close();

  let questionId;

  // SAT Reading
  questionId = 2;
  const readingData = await SATReading.crawl(`http://sat.kmf.com/practice/result/${questionId}/1/reading`);

  // SAT Writing and Language
  questionId = 149;
  const writingData = await SATWriting.crawl(`http://sat.kmf.com/practice/result/${questionId}/1/writing`);

  // SAT Math
  questionId = 217;
  const mathData = await SATMath.crawl(`http://sat.kmf.com/practice/result/${questionId}/1/math`);

  // SAT Essay
  questionId = 225;
  const essayData = await SATEssay.crawl(`http://sat.kmf.com/practice/result/${questionId}/1/essay`);

})();
