const Promise = require("bluebird");
const cheerio = require("cheerio");

const crawler = (browser) => {
  return async (url) => {
    const page = await browser.newPage();
    await page.goto(url);
    const content = await page.content();
    const $ = cheerio.load(content);

    // sectionId
    const sectionId = $("#section_id").val();

    // audios
    const scripts = $("script")
      .filter((i, element) => {
        let $element = $(element);
        return !$element.attr("src") && $element.attr("type") === "text/javascript";
      })
      .map((i, element) => $(element).html())
      .toArray();

    let findMp3 = /"filePath":"http:[^"]*\.mp3"/ig;
    let audios = [];
    let group;
    while ((group = findMp3.exec(scripts)) !== null) {
      audios.push(JSON.parse("{" + group[0] + "}").filePath);
    }

    // paragraphs
    const paragraphs = [];
    let paragraph;
    const _paragraphs = $(".stem-paragraph-list").children();
    for (let i = 0; i < _paragraphs.length; i++) {
      let $element = $(_paragraphs[i]);
      if ($element.hasClass("stem-part-title")) {
        paragraph = {};
        paragraphs.push(paragraph);
        paragraph.title = $element.text().trim();
        paragraph.paragraphs = [];
      } else if ($element.hasClass("stem-paragraph")) {
        let orignal = $element.text().trim();
        let translation = null;
        let $nextElement = $(_paragraphs[i + 1]);
        if ($nextElement && $nextElement.hasClass("translation")) {
          translation = $nextElement.text().trim();
          i += 1;
        }
        paragraph.paragraphs.push({
          orignal: orignal,
          translation: translation
        });
      }
    }

    // questions
    const questions = await Promise.all($(".l-question-num .js-part-dom")
      .map(async (i, element) => {
        const qid = $(element).attr("data-qid");
        const newPage = await browser.newPage();
        await newPage.goto(`http://ielts.kmf.com/subject/ajaxgetquestiondetaildom?section_id=${sectionId}&qid=${qid}`);
        const content = await newPage.content();
        await newPage.close();
        return content;
      })
      .toArray());

    return {
      sectionId: sectionId,
      audios: audios,
      paragraphs: paragraphs,
      questions: questions
    };
  };
};

module.exports = crawler;
