const SAT = require("./sat");

const Essay = {
  __proto__: SAT,

  async crawl(uri) {
    // super crawl
    await super.crawl(uri);
    const $ = this.$;

    // main title and description
    const title = $("h2.exercise").text().trim();
    const description = $(".sat-readtit-ques").text().trim();
    const introduction = $(".sat-readques-intr").text().trim();

    // get essay
    const $essay = this.getEssay();

    // get and remove essay title
    const $essayTitle = $(".kmf-main-head", $essay);
    const essayTitle = $essayTitle.text().trim();
    $essayTitle.remove();

    // get and remove essay reference
    const $reference = $(".sat-essay-part").children("b").first();
    const reference = $reference.text().trim();
    $reference.remove();

    // remove line number
    $(".kmf-paragraph-tips", $essay).remove();

    // images
    const images = this.getImages();

    // split essay to array
    const essay = this.essayToArray()
      .join("\n")
      .split(/\n|\r/)
      .map(x => x.trim())
      .filter(x => x);

    // get explain
    const explain = $(".sat-essay-explain", $essay).text().trim();

    // question
    const question = $(".sat-subject-box .write-tit-box").text().trim();

    return {
      title: title,
      description: description,
      introduction: introduction,
      essay: {
        title: essayTitle,
        reference: reference,
        essay: essay,
        explain: explain
      },
      question: question,
      images: images
    };
  }
};

module.exports = Essay;