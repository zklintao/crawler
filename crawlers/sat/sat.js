const rp = require("request-promise");
const cheerio = require("cheerio");

const SAT = {
  async crawl(uri) {
    let options = {
      uri: uri,
      transform: function (body) {
        return cheerio.load(body);
      }
    };

    this.$ = await rp(options)
      .then(function ($) {
        return $;
      })
      .catch(function (err) {
        console.log(err);
      });
  },

  getImages(satBox = ".sat-read-outbox") {
    const $ = this.$;
    const $satBox = this.getEssay(satBox);

    const images = [];
    $("img", $satBox)
      .each((i, element) => {
        const $element = $(element);
        const index = images.push($element.attr("src")) - 1;
        $element.before(`{{img img-id="${index}"/}}`);
      });
    return images;
  },

  getEssay(essaySelector = ".sat-essay-part") {
    const $ = this.$;
    const $essay = $(essaySelector);
    return $essay;
  },

  essayToArray(essaySelector = ".sat-essay-part") {
    const $ = this.$;
    const $essay = this.getEssay(essaySelector);

    // mark paragraph with indent
    $(".kmf-row-indent", $essay).text("\t");

    // temporarily add AngularJS style markdown
    $(".kmf-num-tips", $essay).before("{{span class=\"marker\"}}").after("{{/span}}");
    $("u", $essay).before("{{u}}").after("{{/u}}");
    $("b", $essay).before("{{b}}").after("{{/b}}");
    $("i", $essay).before("{{i}}").after("{{/i}}");

    // split paragraph and convert AngularJS style markdown to HTML style
    return $essay.text().replace(/{{/g, "<").replace(/}}/g, ">")
      .split("\t")
      .map(x => x.trim())
      .filter(x => x);
  },

  getQuestions(questionSelector = ".sat-subject-box") {
    const $ = this.$;
    const $questions = $(questionSelector);
    return $questions;
  },

  questionsToArray({
    questionSelector = ".sat-subject-box",
    descriptionSelector = ".sat-subtit-ques"
  } = {}) {
    const $ = this.$;
    const $questions = this.getQuestions(questionSelector);

    return $questions
      .map((i, questionElement) => {
        let question = {};
        let $question = $(questionElement);

        // question description
        question.id = parseInt($question.attr("data-subid"));
        question.description = $(descriptionSelector, $question).text().replace(/{{/g, "<").replace(/}}/g, ">").trim();
        question.introduction = $(".sat-subintr-tip", $question).text().replace(/{{/g, "<").replace(/}}/g, ">").trim();

        // choices
        question.choices = $(".sat-sub-choice .sat-sub-option", $question)
          .map((i, choiceElement) => {
            let choice = {};
            let $choice = $(choiceElement);

            // choice description
            choice.id = $(".sub-option", $choice).text().replace(/{{/g, "<").replace(/}}/g, ">").trim();
            choice.description = $(".sub-stem", $choice).text().replace(/{{/g, "<").replace(/}}/g, ">").trim();

            // set correct anwser
            if ($choice.hasClass("sat-resop-yes")) {
              choice.correct = true;
            }

            return choice;
          })
          .toArray();

        return question;
      })
      .toArray();
  }
};

module.exports = SAT;