const SAT = require("./sat");

const Writing = {
  __proto__: SAT,

  async crawl (uri) {
    // super crawl
    await super.crawl(uri);
    const $ = this.$;

    // main title and description
    const title = $("h2.exercise").text().trim();
    const description = $(".sat-readtit-ques").text().trim();
    const introduction = $(".sat-readques-intr").text().trim();

    // get essay
    const $essay = this.getEssay();

    // get and remove essay title
    const $essayTitle = $(".kmf-main-head", $essay);
    const essayTitle = $essayTitle.text().trim();
    $essayTitle.remove();

    // images
    const images = this.getImages();

    // split essay to array
    const essay = this.essayToArray();

    // get explain
    const explain = $(".sat-essay-explain", $essay).text().trim();

    // questions
    const questions = this.questionsToArray();

    return {
      title: title,
      description: description,
      introduction: introduction,
      essay: {
        title: essayTitle,
        essay: essay,
        explain: explain
      },
      questions: questions,
      images: images
    };
  }
};

module.exports = Writing;
