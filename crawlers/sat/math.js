const SAT = require("./sat");

const Math = {
  __proto__: SAT,

  async crawl (uri) {
    // super crawl
    await super.crawl(uri);
    const $ = this.$;

    // main title and description
    const title = $("h2.exercise").text().trim();

    // images
    const images = this.getImages();

    // questions
    const questions = this.questionsToArray({
      descriptionSelector: ".sat-submath-ques"
    });

    return {
      title: title,
      questions: questions,
      images: images
    };
  }
};

module.exports = Math;
