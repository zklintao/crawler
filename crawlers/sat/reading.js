const SAT = require("./sat");

const Reading = {
  __proto__: SAT,

  async crawl (uri) {
    // super crawl
    await super.crawl(uri);
    const $ = this.$;

    // main title and description
    const title = $("h2.exercise").text().trim();
    const description = $(".sat-readtit-ques").text().trim();
    const introduction = $(".sat-readques-intr").text().trim();

    // get essay
    const $essay = this.getEssay();

    // get and remove essay title
    const essayTitle = $(".kmf-main-head", $essay).text().trim();
    $(".kmf-main-head", $essay).remove();

    // remove line number
    $(".kmf-row-tips", $essay).remove();

    // images
    const images = this.getImages();

    // split essay to array
    const essay = this.essayToArray()
      .map(paragraph => paragraph
        .split(/\n|\r/)
        .map(x => x.trim())
        .filter(x => x))
      .filter(x => x.length > 0);

    // get explain
    const explain = $(".sat-essay-explain", $essay).text().trim();

    // questions
    const questions = this.questionsToArray();

    return {
      title: title,
      description: description,
      introduction: introduction,
      essay: {
        title: essayTitle,
        essay: essay,
        explain: explain
      },
      questions: questions,
      images: images
    };
  }
};

module.exports = Reading;
